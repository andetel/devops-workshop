package data;

import java.util.ArrayList;

/**
 * Class for the GroupChat object as saved in database
 */
public class GroupChat {
    private int groupChatId;
    private String groupChatName;
    private ArrayList<Message> messageList = new ArrayList<>();
    private ArrayList<User> userList = new ArrayList<>();

    public GroupChat() {

    }

    public GroupChat(int groupChatId, String groupChatName) {
        this.groupChatId = groupChatId;
        this.groupChatName = groupChatName;
    }

    public int getGroupChatId() {
        return groupChatId;
    }

    public String getGroupChatName() {
        return groupChatName;
    }

    public ArrayList<Message> getMessageList() {
        ArrayList<Message> tmpMessageList = new ArrayList<>();

        for(Message message: this.messageList) {
            tmpMessageList.add(message);
        }

        return tmpMessageList;
    }

    public ArrayList<User> getUserList() {
        ArrayList<User> tmpUserList = new ArrayList<>();

        for(User user: this.userList) {
            tmpUserList.add(user);
        }

        return tmpUserList;
    }

    public void setGroupChatId(int newId) {
        this.groupChatId = newId;
    }

    public void setGroupChatName(String newName) {
        this.groupChatName = newName;
    }

    public void setMessageList(ArrayList<Message> messages) {
        this.messageList = messages;
    }

    public void setUserList(ArrayList<User> users) {
        this.userList = users;
    }
}
