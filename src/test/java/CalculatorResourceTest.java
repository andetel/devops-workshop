import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression));

        expression = " 300-99 ";
        assertEquals(201, calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+1";
        assertEquals(401, calculatorResource.sum(expression));

        expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100-1";
        assertEquals(898, calculatorResource.subtraction(expression));

        expression = "202-2";
        assertEquals(200, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "2*2*2";
        assertEquals(8, calculatorResource.multiplication(expression));

        expression = "4*5";
        assertEquals(20, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "20/2/2";
        assertEquals(5, calculatorResource.division(expression));

        expression = "50/5";
        assertEquals(10, calculatorResource.division(expression));
    }
}
